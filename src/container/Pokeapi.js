import React from 'react';
import Loader from '../components/Loader';
import '../components/PokemonListItem.css';

class Pokeapi extends React.Component {
    state = {
        isLoading: true,
        data: [],
    }
    
    async componentDidMount() {
        this.setState({
            isLoading: true,
        })

        try {
        const res = await fetch('https://pokeapi.co/api/v2/pokemon')
        const data = await res.json();
        this.setState({data, isLoading: false});
        } catch(err) {
            console.log(err);
            this.setState({
                isLoading: false,
            })
            throw err;
        }
    }

    // render() {
    //     const {isLoading, data} = this.state;
    //     return (
    //         <div className="list-item">
    //             <h1>PokeApi</h1>
    //             {isLoading ? <Loader /> : <p>{data.results.name}</p>}
    //             {console.log(data.results)}
    //         </div>
    //     )
    // }
}

export default Pokeapi;
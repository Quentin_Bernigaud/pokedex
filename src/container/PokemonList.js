import React from "react";
import PokemonListItem from "../components/PokemonListItem.js";
import Loader from "../components/Loader";

class PokemonList extends React.Component {
  state = {
    isLoading: true,
    data: []
  };

  async componentDidMount() {
    this.setState({
      isLoading: true
    });

    try {
      const res = await fetch("https://pokeapi.co/api/v2/pokemon");
      const data = await res.json();
      this.setState({ data, isLoading: false });
    } catch (err) {
      console.log(err);
      this.setState({
        isLoading: false
      });
      throw err;
    }
  }

  render() {
    const { isLoading, data } = this.state;
    return (
      <div className="list-item">
        {isLoading ? (
          <Loader />
        ) : (
          data.results.map((name, index) => (
            <PokemonListItem key={index} {...name} index={index+1} />
          ))
        )}
      </div>
    );
  }
}

// const PokemonList = () => (
//   <>
// {results.map((name, index) => (
//   <PokemonListItem key={index} {...name} />
// ))}
//   </>
// );

// const PokemonList = () => (
//     <>
//       {data.pokemon.map((pokemon, index) => (
//         <PokemonListItem
//           key={index}
//           name={pokemon.name}
//           url={pokemon.url}
//         />
//       ))}
//     </>
//   );

export default PokemonList;

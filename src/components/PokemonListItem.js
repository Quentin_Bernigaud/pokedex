import React from "react";
import "./PokemonListItem.css";
import {Link} from "react-router-dom";


const PokemonListItem = ({ name, index }) => (
  <>
    <Link to={"/pokemon/" + name}>
      <div className="item">
        <img
          src={
            "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
            index +
            ".png"
          }
          alt={name}
        ></img>
        <div className="infos">
          <h3>{name}</h3>
          <p>{index}</p>
        </div>
      </div>
    </Link>
  </>
);

// class PokemonListItem extends React.Component {
//   state = {
//     isLoading: true,
//     data: []
//   };

//   async componentDidMount() {
//     this.setState({
//       isLoading: true
//     });

//     try {
//       const res = await fetch(url);
//       const data = await res.json();
//       this.setState({ data, isLoading: false });
//     } catch (err) {
//       console.log(err);
//       this.setState({
//         isLoading: false
//       });
//       throw err;
//     }
//   }

//   render() {
//     const { isLoading, data } = this.state;
//     return (
//       <div className="list-item">
//         {isLoading ? (
//           <Loader />
//         ) : (
//           data.results.map((name, index, url) => (
//             <PokemonListItem key={index} {...name} {...url} />
//           ))
//         )}
//       </div>
//     );
//   }
// }

export default PokemonListItem;

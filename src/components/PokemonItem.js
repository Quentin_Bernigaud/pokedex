import React, { Fragment } from "react";
import "./PokemonItem.css";
// import { Link } from "react-router-dom";
import PokemonFetch from "./PokemonFetch";
import "./PokemonItem.css";
// import PokeTypes from "./PokeTypes";

const PokemonItem = props => (
  <Fragment>
    <div className="single-item">
      <h1>{props.name}</h1>
      <div>
      <div className="pokemon-images">
        <img src={props.sprites.front_default} alt={props.name}></img>
        <img src={props.sprites.back_default} alt={props.name}></img>
        <img src={props.sprites.front_shiny} alt={props.name}></img>
        <img src={props.sprites.back_shiny} alt={props.name}></img>
        </div>
        <div className="pokemon-info">
          <div>
            <h2>Height</h2>
            <p>{props.height*10} cm</p>
          </div>
          <div>
            <h2>Weight</h2>
            <p>{props.weight/10} Kg</p>
          </div>
          <div>
            <h2>Type</h2>
            {props.types.map(item => (
            //   <span style={"background-color: #"+PokeTypes[item.type.name]}>{item.type.name}</span>
              <span>{item.type.name} </span>
            ))}
          </div>
        </div>
      </div>
      <div className="moves">
      </div>
    </div>
  </Fragment>
);

export default PokemonFetch(
  props => "https://pokeapi.co/api/v2/pokemon/" + props.match.params.name,
  10
)(PokemonItem);

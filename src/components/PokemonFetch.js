import React, {Component} from 'react';
import Api from '../container/Api';
import Loader from './Loader';

const PokemonFetch = getUrl => InnerComponent =>
  class PokemonFetch extends Component {
    constructor(props) {
      super(props);
      this.props = props;

      this.state = {
        data: {},
        error: null,
        isLoading: true,
      };
    }

    deepEqual(a, b) {
        if (
          typeof a === 'object' &&
          a != null &&
          (typeof b === 'object' && b != null)
        ) {
          const count = [0, 0];
          //eslint-disable-next-line no-unused-vars
          for (const _ in a) {
            count[0]++;
          }
          //eslint-disable-next-line no-unused-vars
          for (const _ in b) {
            count[1]++;
          }
          if (count[0] - count[1] !== 0) {
            return false;
          }
          for (const key in a) {
            if (!(key in b) || !this.deepEqual(a[key], b[key])) {
              return false;
            }
          }
          for (const key in b) {
            if (!(key in a) || !this.deepEqual(b[key], a[key])) {
              return false;
            }
          }
          return true;
        }
        return a === b;
      };

    componentDidMount() {
      this.fetchData();
    }
    componentDidUpdate(prevProps) {
      if (!this.deepEqual(this.props, prevProps)) {
        this.fetchData(this.props.userID);
      }
    }

    fetchData() {
      this.setState({isLoading: true});

      Api(getUrl(this.props))
        .then(data => this.setState({data, isLoading: false}))
        .catch(error => this.setState({error, isLoading: false}));
    }

    render() {
      if (this.state.error) {
        return (
          <div className="notification is-danger">
            {this.state.error.message}
          </div>
        );
      }

      if (this.state.isLoading) {
        return <Loader />;
      }
      return <InnerComponent {...this.props} {...this.state.data} />;
    }
  };

export default PokemonFetch;

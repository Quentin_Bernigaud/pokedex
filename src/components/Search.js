import React from 'react';
import './Search.css';

const Search = ({handleSubmit}) => (
    <>
      <form onSubmit={handleSubmit}>
        <input id="search" placeholder="Search..." name="search" type="text" />
        <input type="submit" className="fa-input" value="&#xf002;"/>
      </form>
    </>
  );

export default Search;
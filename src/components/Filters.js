import React from "react";
import './Filters.css';

const Filters = () => (
  <>
    <div className="filters">
        <div>
      <select name="region" id="region-select">
        <option value="">Select a region</option>
        <option value="kanto">Kanto</option>
        <option value="johto">Johto</option>
        <option value="hoenn">Hoenn</option>
        <option value="sinnoh">Sinnoh</option>
        <option value="unys">Unys</option>
        <option value="kalos">Kalos</option>
        <option value="alola">Alola</option>
        <option value="galar">Galar</option>
      </select>
      <select name="type" id="type-select">
        <option value="">Select a type</option>
        <option value="normal">Normal</option>
        <option value="fire">Fire</option>
        <option value="water">Water</option>
        <option value="grass">Grass</option>
        <option value="electric">Electric</option>
        <option value="ice">Ice</option>
        <option value="fighting">Fighting</option>
        <option value="poison">Poison</option>
        <option value="ground">Ground</option>
        <option value="flying">Flying</option>
        <option value="psychic">Psychic</option>
        <option value="bug">Bug</option>
        <option value="rock">Rock</option>
        <option value="ghost">Ghost</option>
        <option value="dark">Dark</option>
        <option value="dragon">Dragon</option>
        <option value="steel">Steel</option>
        <option value="fairy">Fairy</option>
      </select>
      </div>
      <select name="order" id="order-select">
        <option value="">Order by</option>
        <option className="fa-option" value="asc-id">&#xf15d; | Number</option>
        <option className="fa-option" value="desc-id">&#xf15e; | Number</option>
        <option className="fa-option" value="asc-alpha">&#xf15d; | Name</option>
        <option className="fa-option" value="desc-alpha">&#xf15e; | Name</option>
      </select>
    </div>
  </>
);

export default Filters;

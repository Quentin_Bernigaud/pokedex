import React from 'react';
import { Link } from "react-router-dom";
import Search from './Search.js';
import "./Header.css";

const Header = () => (
    <>
        <header className="App-header">
            <nav>
                <Link to="/"><img src="https://upload.wikimedia.org/wikipedia/commons/9/98/International_Pok%C3%A9mon_logo.svg" alt="Logo"></img></Link>
                <Search/>
            </nav>
      </header>
    </>
);

// class Header extends React.Component {
//     state = {
//         value: ""
//       };
    
//       handleSubmit = ev => {
//         ev.preventDefault();
//         const { value } = ev.currentTarget.filter;
    
//         this.setState({
//           value
//         });
//       };
// }

export default Header;
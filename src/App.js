import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Header from "./components/Header";
import Filters from "./components/Filters";
import PokemonList from "./container/PokemonList";
import PokemonItem from "./components/PokemonItem";
import Loader from "./components/Loader";
class App extends React.Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Header />


          <Switch>
            <Route exact path="/">
              <Filters />
              <PokemonList/>
            </Route>


            <Route path="/pokemon/:name" component={PokemonItem}></Route>
            <Route>Error 404</Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
